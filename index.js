const ArtefactServer = require('artefact-server')
const { artefactEncryptionKey } = require('artefact-store')
const path = require('path')
const Obz = require('obz')
const defer = require('pull-defer')
const toPull = require('stream-to-pull-stream')
const { v4: uuid } = require('uuid') // TODO check which version to use!
const fs = require('fs')

const Prune = require('./prune')
const AutoPrune = require('./auto-prune')
const { autoPrune: defaultAutoPruneConfig } = require('./lib/defaults')

module.exports = {
  name: 'hyperBlobs',
  version: require('./package.json').version,
  manifest: {
    onReady: 'async',
    add: 'sink',
    driveAddress: 'async',
    registerDrive: 'async',
    prune: 'async',
    autoPrune: {
      set: 'async',
      get: 'async'
    }
  },
  permissions: {
    anonymous: {
      allow: ['driveAddress']
    }
  },
  init (ssb, config) {
    const {
      port = 26836,
      pataka = false,
      storeOpts = {}
    } = (config.hyperBlobs || {})

    const artefactServerObz = Obz()
    ArtefactServer({
      storage: path.resolve(config.path, 'hyperBlobs'),
      port,
      pataka,
      storeOpts
    })
      .then(_server => artefactServerObz.set(_server))
      .catch(err => { throw err })

    ssb.close.hook(function (close, args) {
      const self = this
      artefactServerObz.once(artefactServer => {
        artefactServer.close()
          .then(() => close.apply(self, args))
      })
    })

    ssb.on('rpc:connect', remote => {
      if (remote.id === ssb.id) return // could be ssb-client connection
      if (!remote.hyperBlobs || !remote.hyperBlobs.driveAddress) return

      if (!pataka) {
        // add remote drive, as we're only doing sparse replication!
        remote.hyperBlobs.driveAddress((err, remoteDriveAddress) => {
          if (err) {
            if (remote.closed) return reconnect(remote.stream.address)
            if (err.message.match(/end of parent stream/)) return reconnect(remote.stream.address)
            console.error(err)
            return
          }
          if (!remoteDriveAddress) {
            if (remoteDriveAddress === null) return // is a pataka
            console.log('no remoteDriveAddress found')
            return
          }

          // NOTE we call registerDrive via the ssb object so that this call
          // will be causght by hooks on this fn (used in tests!)
          ssb.hyperBlobs.registerDrive(remoteDriveAddress, (err) => {
            if (err) console.error(err)
          })
        })
      }
      if (pataka) {
        // check if we follow this remote, as we're doing doing FULL replication
        ssb.friends.isFollowing({ source: ssb.id, dest: remote.id }, (err, isFollowing) => {
          if (err) {
            console.error(err)
            return
          }
          if (!isFollowing) return
          // pataka will not be a backup for people it does not follow

          remote.hyperBlobs.driveAddress((err, remoteDriveAddress) => {
            if (err) {
              if (remote.closed) return reconnect(remote.stream.address)
              if (err.message.match(/end of parent stream/)) return reconnect(remote.stream.address)
              console.error(err)
              return
            }
            if (!remoteDriveAddress) {
              if (remoteDriveAddress === null) return // is a pataka
              console.log('no remoteDriveAddress found')
              return
            }

            // NOTE we call registerDrive via the ssb object so that this call
            // will be causght by hooks on this fn (used in tests!)
            ssb.hyperBlobs.registerDrive(remoteDriveAddress, (err) => {
              if (err) console.error(err)
            })
          })
        })
      }
    })

    const retryCounter = {
      // [RemoteId]: Integer
    }
    function reconnect (multiAddress, attempts = 7) {
      if (!ssb.conn) {
        console.error('ssb-hyper-blobs needs ssb-conn to restart closed connections')
        return
      }

      if (retryCounter[multiAddress] === undefined) retryCounter[multiAddress] = attempts
      retryCounter[multiAddress] = retryCounter[multiAddress] - 1

      if (retryCounter[multiAddress] < 0) {
        console.warn('ssb-hyperblobs reconnect not helping, giving up')
        delete retryCounter[multiAddress]
        return
      }

      console.log(`ssb-hyper-blobs retrying, ${retryCounter[multiAddress] + 1} attempts remaining...`)
      ssb.conn.disconnect(multiAddress, (err) => {
        if (err) {
          console.log('ssb.conn.disconnect error', err)
          return setTimeout(() => reconnect(multiAddress), 1000)
        }

        setTimeout(
          () => ssb.conn.connect(multiAddress, (err) => {
            if (err) {
              console.log('ssb.conn.connect error', err)
              reconnect(multiAddress)
            }
          }),
          3000
        )
      })
    }

    //   const peerAttempts = {
    //     // peerId: attemptCount
    //   }
    //   ssb.on('rpc:connect', remote => {
    //     if (remote.id === ssb.id) return // could be ssb-client connection
    //     if (!remote.hyperBlobs) return

    //     remote.driveAddress((err, address) => {
    //       if (err) throw err

    //       peerAttempts[remote.id] = peerAttempts[remote.id] || 0
    //       if (remote.closed) return reconnect(remote)
    //       // NOTE: the artefact-store takes a while to get ready
    //       // the rpc connecting can closes if there's is no action, here we manually start the connection again

    //       attemptRegister(remote, address)
    //     })
    //   })

    //   function reconnect (remote) {
    //     if (!ssb.conn) {
    //       console.error('ssb-hyper-blobs needs ssb-conn to restart closed connections')
    //       return
    //     }
    //     ssb.conn.connect(remote.stream.address)
    //   }
    //   function attemptRegister (remote, address) {
    //     peerAttempts[remote.id] = peerAttempts[remote.id] + 1

    //     remote.hyperBlobs.registerDrive(address, (err) => {
    //       if (!err) {
    //         delete peerAttempts[remote.id]
    //         return
    //       }
    //       if (peerAttempts[remote.id] < 5) {
    //         console.log(`remote.hyperBlobs.registerDrive failed (attempt ${peerAttempts[remote.id]})`)
    //         if (remote.closed) return reconnect(remote)
    //         return setTimeout(() => attemptRegister(remote, address), 2000)
    //       }

    //       console.error('hyperBlobs.registerDrive failed. Giving up', err)
    //     })
    //   }
    // }

    let abortAutoPrune = AutoPrune(ssb, artefactServerObz)

    const API = {
      onReady (cb) {
        artefactServerObz.once((as) => {
          cb(null, process.env.NODE_ENV === 'test' ? as : true)
        })
      },
      add (cb) {
        const sink = defer.sink()

        const blobId = uuid()
        const readKey = artefactEncryptionKey()
        const opts = {}

        artefactServerObz.once(artefactServer => {
          const writeStream = artefactServer.store.createWriteStream(blobId, readKey, opts)

          const _sink = toPull.sink(writeStream, (err) => {
            if (err) return cb(err)

            artefactServer.store.fileReady(blobId)
              .then(() => {
                cb(null, {
                  driveAddress: toString(artefactServer.store.driveAddress),
                  blobId,
                  readKey: toString(readKey)
                  // fileName....
                })
              })
          })
          sink.resolve(_sink)
        })

        return sink
      },
      driveAddress (cb) {
        if (pataka) return cb(null, null)

        artefactServerObz.once(artefactServer => {
          cb(null, toString(artefactServer.store.driveAddress))
        })
      },
      registerDrive (address, cb) {
        artefactServerObz.once(artefactServer => {
          artefactServer.store.addDrive(address)
            .then(() => cb(null, true))
            .catch(err => cb(err))
        })
      },
      prune (opts, cb) {
        artefactServerObz.once(as => {
          Prune(as)(opts, cb)
        })
      },
      autoPrune: {
        set (value, cb) {
          if (!isValidConfig(value)) return cb(new Error('invalid autoPrune config'))

          ssb.config.hyperBlobs = ssb.config.hyperBlobs || {}
          ssb.config.hyperBlobs.autoPrune = value

          abortAutoPrune()
          abortAutoPrune = AutoPrune(ssb, artefactServerObz)

          persistAutoPruneConfig(path.join(ssb.config.path, 'config'), value, (err) => {
            err ? cb(err) : cb(null)
          })
        },
        get (cb) {
          const config = ssb.config.hyperBlobs && ssb.config.hyperBlobs.autoPrune

          if (!config) return cb(null, null) // autoPrune: null|undefined
          if (config === true) return cb(null, defaultAutoPruneConfig) // autoPrune: true

          // autoPrune: {...}
          return cb(null, { ...defaultAutoPruneConfig, ...config })
        }
      }
    }

    return API
  }
}

function isValidConfig (value) {
  if (typeof value === 'boolean') return true

  if (typeof value === 'object') {
    if (value.maxRemoteSize && typeof value.maxRemoteSize !== 'number') return false
    if (value.intervalTime && typeof value.intervalTime !== 'number') return false
    if (value.startDelay && typeof value.startDelay !== 'number') return false
    return true
  }

  return false
}

function toString (buf) {
  if (typeof buf === 'string') return buf
  return buf.toString('hex')
}

function persistAutoPruneConfig (configPath, value, cb) {
  fs.readFile(configPath, 'utf8', (err, raw) => {
    if (err) {
      if (!err.message.match(/no such file/)) return cb(err)
      // fine if the file doesn't exist, we can make it
    }

    const config = raw ? JSON.parse(raw) : {}
    config.hyperBlobs = config.hyperBlobs || {}
    config.hyperBlobs.autoPrune = value

    fs.writeFile(configPath, JSON.stringify(config, null, 2), cb)
  })
}
