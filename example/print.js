const fs = require('fs')
const path = require('path')

const file = fs.readFileSync(path.resolve(__dirname, './index.js'), 'utf8')

const rmRegex = /\s*\/\/\srm\s*/
const uncommentRegex = /\s*\/\/\suncomment\s*/
const toggleIndentRegex = /\s*\/\/\sblock-<\s*/

let indent = 0

const output = file
  .split('\n')
  .reduce((acc, line) => {
    if (line.match(rmRegex)) return acc
    if (line.match(toggleIndentRegex)) {
      indent = indent ? 0 : -2 // could determine how much to indent, and direction
      return acc
    }
    if (line.match(uncommentRegex)) {
      line = line
        .replace(uncommentRegex, '')
        .replace(/\/\/\s/, '')
    }
    if (indent) line = line.replace(/^\s\s/, '')

    acc.push(line)
    return acc
  }, [])
  .join('\n')

console.log('\n\n====================================================')
console.log('Printing example code for README!')
console.log('====================================================\n\n')
console.log(output)
