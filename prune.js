const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const sort = require('pull-sort')

const IsPrunable = require('./lib/is-prunable-file')

module.exports = function Prune (artefactServer) {
  return function prune (opts = {}, cb) {
    /*
      opts = {
        pruneSize: Number,
        minSize: Number,
        maxSize: Number,
        minDate: Date|Number,
        maxDate: Date|Number,
        sort: Function
      }
    */

    const pruneFile = PruneFile(artefactServer)
    const isPrunable = IsPrunable(opts)
    // NOTE we must instantiate isPrunable here because it closes over a counter

    pull(
      pullFiles(artefactServer),
      sort(opts.sort || sortByLastAccessedDesc),
      pull.filter(isPrunable),
      paraMap(pruneFile, 6),
      pull.collect((err, prunedFiles) => {
        if (err) return cb(err)
        cb(null, prunedFiles)
      })
    )
  }
}

function pullFiles (artefactServer) {
  const getFileStats = GetFileStats(artefactServer)

  return pull(
    pull.values(artefactServer.store.remoteDrives),
    pull.map(drive => drive.key),
    paraMap((driveAddress, cb) => {
      artefactServer.store.getFilesByDrive('/', driveAddress)
        .catch(err => cb(err))
        .then(files => cb(null, { driveAddress, files })) // here we save the driveAddress as well, its needed for later
    }, 6),
    paraMap(getFileStats, 6),
    pull.flatten()
  )
}

function GetFileStats (artefactServer) {
  return function ({ driveAddress, files }, cb) {
    pull(
      pull.values(files),
      paraMap((filename, cb) => {
        artefactServer.store.fileStats(filename, driveAddress)
          .catch(err => cb(err))
          .then(stats => cb(null, { filename, driveAddress, ...stats }))
      }, 6),
      pull.collect((err, files) => {
        if (err) return cb(err)
        cb(null, files)
      })
    )
  }
}

function PruneFile (artefactServer) {
  return function pruneFile (file, cb) {
    const { driveAddress, filename } = file

    artefactServer.store.remove(filename, driveAddress)
      .catch(err => cb(err))
      .then(() => cb(null, file))
  }
}

function sortByLastAccessedDesc (fileA, fileB) {
  return fileA.atime - fileB.atime
}
