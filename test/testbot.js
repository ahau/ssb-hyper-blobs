const Server = require('scuttle-testbot')
const Config = require('ssb-config/defaults')

module.exports = function TestBot (opts = {}) {
  const stack = Server

  stack
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/ebt'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-db2/compat/log-stream'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-conn'))
    .use(require('ssb-replicate'))
    .use(require('ssb-friends'))
    .use(require('../')) // ssb-hyper-blobs

  opts.port = 15000 + Math.floor(Math.random() * 2e3)
  opts.hyperBlobs = opts.hyperBlobs || {}
  opts.hyperBlobs.port = 45000 + Math.floor(Math.random() * 2e3)
  opts.hyperBlobs.storeOpts = Object.assign(
    // { bootstrap: [] },
    // TODO - ideally we could put in no bootstrap DHT or a local mock one...
    opts.hyperBlobs.storeOpts || {}
  )

  // NOTE we use config/defaults below because it stabilises connections,
  // it also:
  //   - requires keys or it creates them (and persists them, sigh)
  //   - makes it's own name and path (which we want scuttle-testbot to handle)
  //
  // hence this weird patching
  opts.keys = 'dummy'
  const config = Config('dummy', opts)
  delete config.keys
  delete config.path

  return stack(config)
}
