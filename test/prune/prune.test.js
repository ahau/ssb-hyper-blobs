const test = require('tape')

const path = require('path')
const file = require('pull-file')
const crypto = require('crypto')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { promisify: p } = require('util')
const fs = require('fs')
const { connect } = require('scuttle-testbot')
const stat = p(fs.stat)

const IsPrunable = require('../../lib/is-prunable-file')
const Server = require('../testbot')
const file1Path = path.resolve(__dirname, '../../README.md')
const file2Path = path.resolve(__dirname, '../../package.json')

test('hyperBlobs.prune', t => {
  const registered = new Set()
  const isFinished = () => registered.size === 2
  const caps = {
    shs: crypto.randomBytes(32).toString('base64')
  }

  let bob

  const alice = Server({
    caps,
    hyperBlobs: {
      port: 61001
    }
  })
  alice.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
    bob.hyperBlobs.driveAddress((_, bobDrive) => {
      const [address, cb] = args
      registerDrive.call(this, address, err => {
        if (err) return cb(err)
        if (isFinished()) return cb(err)

        t.equal(address, bobDrive, 'bob remotely calls alice to register his driveAddress')
        next(address)
        cb(err)
      })
    })
  })

  // NOTE here we simulate alice being completely ready
  // (has been running for 10 seconds, so hyperdrive completely ready)
  // while bob is still starting his systems up when they initially connect.
  //
  // this puts the system in a state where some RPC things might timeout while driveAddress
  // is determined, which might force us to have to reconnect to ensure drive is correctly registered
  const DELAY = 10e3

  setTimeout(() => {
    bob = Server({
      caps,
      hyperBlobs: {
        port: 61002
      }
    })

    bob.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
      alice.hyperBlobs.driveAddress((_, aliceDrive) => {
        const [address, cb] = args
        registerDrive.call(this, address, err => {
          if (err) return cb(err)
          if (isFinished()) return cb(err)

          t.equal(address, aliceDrive, 'alice remotely calls bob to register his driveAddress')
          next(address)
          cb(err)
        })
      })
    })

    connect([alice, bob], {}, (err) => {
      if (err) throw err
    })
  }, DELAY)

  function next (address) {
    if (registered.has(address)) return

    registered.add(address)
    if (isFinished()) {
      runTests()
    }
  }

  function AddFileToAlice (filePath, cb) {
    pull(
      file(filePath),
      alice.hyperBlobs.add((err, data = {}) => {
        t.error(err, 'adds file')

        cb(null, data)
      })
    )
  }

  async function runTests () {
    const addFileAlice = p(AddFileToAlice)

    const { size: file1Size } = await stat(file1Path)
    const { size: file2Size } = await stat(file2Path)

    // alice adds a couple of files
    const file1 = await addFileAlice(file1Path)
    const file2 = await addFileAlice(file2Path)

    // get the smaller file of the two
    // NOTE: cant hard code this because its likely the file sizes will change in the future
    const { largerFile, size } = file1Size > file2Size
      ? { largerFile: file1, size: file1Size }
      : { largerFile: file2, size: file2Size }

    // lets make it so it will only choose the bigger file
    const opts = {
      // lets sort the files from biggest to smallest
      sort: (a, b) => b.size - a.size,
      pruneSize: size + 1
    }

    // need to make sure bob has alice's file to even be pruning!
    alice.hyperBlobs.driveAddress((_, aliceDriveAddress) => {
      bob.hyperBlobs.onReady((_, artefactServer) => {
        // get bob's local copy of alice's drive
        const aliceDrive = artefactServer.store.drives.remote.get(aliceDriveAddress)

        // setting path to '/' should download all files!
        aliceDrive.download('/', (err, data) => {
          t.error(err, 'ensure bob has downloaded all of alices files')
          console.log(data)

          // bob needs to clear space
          bob.hyperBlobs.prune(opts, (err, prunedFiles) => {
            t.error(err, 'prune doesnt throw any errors')

            t.equals(prunedFiles.length, 1, 'only pruned one file')
            if (prunedFiles.length) {
              t.equals(prunedFiles[0].filename, largerFile.blobId, 'file matches the larger one')
            }

            alice.close()
            bob.close()
            t.end()
          })
        })
      })
    })
  }
})

test('lib/is-prunable-file', async t => {
  t.throws(() => IsPrunable({ pruneSize: -5 }), /pruneSize wasnt a valid number/, 'pruneSize >= 0')
  t.throws(() => IsPrunable({ pruneSize: 'dog' }), /pruneSize wasnt a valid number/, 'pruneSize number')

  t.throws(() => IsPrunable({ minSize: -4 }), /minSize wasnt a valid number/, 'minSize >= 0')
  t.throws(() => IsPrunable({ minSize: 'dog' }), /minSize wasnt a valid number/, 'minSize is a number')
  t.throws(() => IsPrunable({ minSize: null }), /minSize wasnt a valid number/, 'minSize not null')

  t.throws(() => IsPrunable({ maxSize: -4 }), /maxSize wasnt a valid number/, 'maxSize >= 0')
  t.throws(() => IsPrunable({ maxSize: 'dog' }), /maxSize wasnt a valid number/, 'maxSize is a number')

  t.throws(() => IsPrunable({ minDate: -4 }), /minDate wasnt a number or date/, 'minDate >= 0')
  t.throws(() => IsPrunable({ minDate: 'dog' }), /minDate wasnt a number or date/, 'minDate is number/ date')

  t.throws(() => IsPrunable({ maxDate: -4 }), /maxDate wasnt a number or date/, 'minDate >= 0')
  t.throws(() => IsPrunable({ maxDate: 'dog' }), /maxDate wasnt a number or date/, 'minDate is number/ date')

  // NOTE - we aren't checking Max > Min anywhere... but that's on developers I think!

  const sharedFileAttrs = {
    dev: 0,
    nlink: 1,
    rdev: 0,
    blksize: 0,
    ino: 0,
    mode: 33188,
    uid: 0,
    gid: 0,
    offset: 1,
    byteOffset: 5689,
    blocks: 1,
    linkname: null,
    mount: null,
    metadata: {}
  }

  const aliceDriveAddress = DriveAddress()
  const bobDriveAddress = DriveAddress()

  const testFiles = [
    {
      filename: '95b34483-7150-4cde-b7bf-a5a882ec719f',
      driveAddress: aliceDriveAddress,
      size: 6578,
      atime: new Date('2021-05-20'),
      mtime: new Date('2021-05-20'),
      ctime: new Date('2021-05-20'),
      ...sharedFileAttrs
    },
    {
      filename: 'e848829e-90f2-47d5-a698-343bb1a6f449',
      driveAddress: bobDriveAddress,
      size: 123123,
      atime: new Date('2021-03-15'),
      mtime: new Date('2021-03-15'),
      ctime: new Date('2021-03-15'),
      ...sharedFileAttrs
    },
    {
      filename: '95b34483-7150-4cde-b7bf-a5a882ec719f',
      driveAddress: aliceDriveAddress,
      size: 500,
      atime: new Date('2021-05-26'),
      mtime: new Date('2021-05-26'),
      ctime: new Date('2021-05-26'),
      ...sharedFileAttrs
    },
    {
      filename: 'e848829e-90f2-47d5-a698-343bb1a6f449',
      driveAddress: aliceDriveAddress,
      size: 10000,
      atime: new Date('2021-05-01'),
      mtime: new Date('2021-05-01'),
      ctime: new Date('2021-05-01'),
      ...sharedFileAttrs
    }
  ]

  const tests = [
    {
      opts: {},
      expectedOutput: testFiles
    },
    {
      opts: { minSize: 501 },
      expectedOutput: [
        testFiles[0],
        testFiles[1],
        testFiles[3]
      ]
    },
    {
      opts: { maxSize: 501 },
      expectedOutput: [
        testFiles[2]
      ]
    },
    {
      opts: { minSize: 500, maxSize: 6578 },
      expectedOutput: [
        testFiles[0],
        testFiles[2]
      ]
    },
    {
      opts: { minDate: new Date('2021-05-26') },
      expectedOutput: [
        testFiles[2]
      ]
    },
    {
      opts: { minDate: new Date('2021-05-01') },
      expectedOutput: [
        testFiles[0],
        testFiles[2],
        testFiles[3]
      ]
    },
    {
      opts: { minDate: new Date('2021-05-01'), maxDate: new Date('2021-05-20') },
      expectedOutput: [
        testFiles[0],
        testFiles[3]
      ]
    },
    // same test but with numbers instead of dates:
    {
      opts: { minDate: Number(new Date('2021-05-01')), maxDate: Number(new Date('2021-05-20')) },
      expectedOutput: [
        testFiles[0],
        testFiles[3]
      ]
    },
    {
      opts: { minDate: new Date('2021-05-20'), minSize: 5000 },
      expectedOutput: [
        testFiles[0]
      ]
    },
    {
      opts: { pruneSize: 10000 },
      expectedOutput: [
        testFiles[0],
        testFiles[2]
      ]
    },
    {
      opts: { pruneSize: 600 },
      expectedOutput: [
        testFiles[2]
      ]
    },
    {
      opts: { pruneSize: 0 },
      expectedOutput: []
    },
    {
      opts: { pruneSize: 10000, maxDate: new Date('2021-05-01') },
      expectedOutput: [
        testFiles[3]
      ]
    }
  ]

  function pullFilesToPrune (files, opts, cb) {
    const isPrunable = IsPrunable(opts)

    pull(
      pull.values(files),
      pull.filter(isPrunable),
      pull.collect(cb)
    )
  }

  pull(
    pull.values(tests),
    paraMap(({ opts }, cb) => pullFilesToPrune(testFiles, opts, cb)),
    pull.collect((err, outputs) => {
      t.error(err, 'runs prune tests without errors')

      outputs.forEach((output, i) => {
        t.deepEqual(output, tests[i].expectedOutput, `prunableFiles returns expected output for ${JSON.stringify(tests[i].opts, null, 2)}`)
      })

      t.end()
    })
  )
})

function DriveAddress () {
  return Buffer.from(
    crypto.randomBytes(16).toString('hex')
  )
}
