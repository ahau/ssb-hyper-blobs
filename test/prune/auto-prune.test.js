const test = require('tape')

const path = require('path')
const file = require('pull-file')
const crypto = require('crypto')
const pull = require('pull-stream')
const { promisify: p } = require('util')
const { connect } = require('scuttle-testbot')

const Server = require('../testbot')
const file1Path = path.resolve(__dirname, '../../README.md')
const file2Path = path.resolve(__dirname, '../../package.json')

if (process.env.NODE_ENV !== 'test') {
  throw new Error('must be NODE_ENV=test to access artefactServer via onReady')
}

test('hyperBlobs.config.autoPrune', t => {
  const registered = new Set()
  const isFinished = () => registered.size === 2
  const caps = {
    shs: crypto.randomBytes(32).toString('base64')
  }

  let bob

  const alice = Server({
    caps,
    hyperBlobs: {
      port: 61001
    }
  })
  alice.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
    bob.hyperBlobs.driveAddress((err, bobDrive) => {
      t.error(err, 'Got Bobs drive address')
      const [address, cb] = args
      registerDrive.call(this, address, err => {
        if (err) return cb(err)
        if (isFinished()) return cb(err)

        t.equal(address, bobDrive, 'bob remotely calls alice to register his driveAddress')
        next(address)
        cb(err)
      })
    })
  })

  // NOTE here we simulate alice being completely ready
  // (has been running for 10 seconds, so hyperdrive completely ready)
  // while bob is still starting his systems up when they initially connect.
  //
  // this puts the system in a state where some RPC things might timeout while driveAddress
  // is determined, which might force us to have to reconnect to ensure drive is correctly registered
  const DELAY = 10e3

  setTimeout(() => {
    bob = Server({
      caps,
      hyperBlobs: {
        port: 61002,
        autoPrune: {
          startDelay: 10e3, // give time for things to start
          intervalTime: 5e3,
          maxRemoteSize: 1 // dont want to keep any
        }
      }
    })

    bob.hyperBlobs.registerDrive.hook(function (registerDrive, args) {
      alice.hyperBlobs.driveAddress((_, aliceDrive) => {
        const [address, cb] = args
        registerDrive.call(this, address, err => {
          if (err) return cb(err)
          if (isFinished()) return cb(err)

          t.equal(address, aliceDrive, 'alice remotely calls bob to register his driveAddress')
          next(address)
          cb(err)
        })
      })
    })

    connect([alice, bob], {}, (err) => {
      if (err) throw err
    })
  }, DELAY)

  function next (address) {
    if (registered.has(address)) return

    registered.add(address)
    if (isFinished()) {
      runTests()
    }
  }

  function AddFileToAlice (filePath, cb) {
    pull(
      file(filePath),
      alice.hyperBlobs.add((err, data = {}) => {
        t.error(err, 'adds file')

        cb(null, data)
      })
    )
  }

  async function runTests () {
    const addFileAlice = p(AddFileToAlice)

    // alice adds a couple of files
    await addFileAlice(file1Path)
    await addFileAlice(file2Path)

    alice.hyperBlobs.driveAddress((err, aliceDriveAddress) => {
      t.error(err, 'Got drive address')
      bob.hyperBlobs.onReady(async (err, artefactServer) => {
        t.error(err, 'Got artefact server')
        const aliceDrive = artefactServer.store.drives.remote.get(aliceDriveAddress)
        // bob's record of alice's drive
        const sizeBefore = await artefactServer.store.driveSize(aliceDriveAddress)
        t.equal(sizeBefore, 0, 'bobs driveSize for alice == 0')

        // setting path to '/' should download all files!
        aliceDrive.download('/', async (err, done) => {
          t.error(err, 'bob downloads all alices files')

          const sizeFull = await artefactServer.store.driveSize(aliceDriveAddress)
          t.true(sizeFull > 8000, 'bobs driveSize for alice now > 8000')
          if (sizeFull <= 8000) console.log('error', { sizeFull })

          checkAutoPruned()

          let count = 0
          async function checkAutoPruned () {
            const sizePruned = await artefactServer.store.driveSize(aliceDriveAddress)

            if (sizePruned === sizeFull) {
              console.log('  waiting for prune...')
              if (++count > 7) {
                t.fail('failed to auto-prune')
                end()
                return
              }

              setTimeout(checkAutoPruned, 3000)
              return
            }
            // NOTE there's some weird behavior happening here
            // - sometimes seeing
            //    - log: ssb-hyper-blobs: auto-pruned 8165 bytes
            //    - log:   waiting for prune... (i.e. driveSize hasn't changed)
            // - then see
            //    - log: ssb-hyper-blobs: auto-pruned 1377 bytes
            //    - test: ✔ successful auto-prune (i.e. driveSize did change)

            t.true(sizePruned < sizeFull, 'successful auto-prune')
            end()
          }
        })
      })
    })
  }

  function end () {
    alice.close()
    bob.close()
    t.end()
  }
})
